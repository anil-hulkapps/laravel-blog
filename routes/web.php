<?php

use App\Http\Controllers\Auth\ProfileController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FollowerController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware'=>'auth'],function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::view('dashboard','dashboard')->name('dashboard');

    Route::group(['prefix'=>'change-password'], function () {
        Route::view('/','auth.passwords.change')->name('change_password');
        Route::put('/',[ProfileController::class,'changePassword'])->name('change');
    });

    Route::group(['prefix'=>'profile', 'as'=>'profile.'], function () {
        Route::view('/','profile.index')->name('index');
        Route::get('/edit',[ProfileController::class,'index'])->name('edit');
        Route::post('/',[ProfileController::class,'store'])->name('store');
        Route::get('/avatar',[ProfileController::class,'removeAvatar'])->name('avatar.remove');

    });

    Route::group(['prefix'=>'posts', 'as'=>'post.'], function () {
        Route::get('/', [PostController::class,'index'])->name('index');
        Route::get('/create', [PostController::class,'create'])->name('create');
        Route::post('/store', [PostController::class,'store'])->name('store');

        Route::group(['prefix'=>'{post}'] ,function () {
            Route::get('/edit', [PostController::class,'edit'])->name('edit');
            Route::put('/', [PostController::class,'update'])->name('update');
            Route::get('/', [PostController::class,'show'])->name('show');
            Route::delete('/', [PostController::class,'destroy'])->name('destroy');
        });
    });

    Route::group(['middleware'=>'is_adult', 'prefix'=>'comments', 'as'=>'comment.'], function () {
        Route::post('/store', [CommentController::class, 'store'])->name('store');
        Route::post('/reply/store', [CommentController::class, 'replyStore'])->name('reply.add');

        Route::group(['prefix'=>'{comment}'], function () {
            Route::get('/edit', [CommentController::class, 'edit'])->name('edit');
            Route::put('/', [CommentController::class, 'update'])->name('update');
            Route::delete('/', [CommentController::class, 'destroy'])->name('delete');
        });

    });

    Route::group(['prefix'=>'notifications', 'as'=>'notifications.'], function () {
        Route::get('/', [NotificationController::class,'index'])->name('index');
        Route::get('/{notification}', [NotificationController::class,'markAsRead'])->name('read.mark');
        Route::delete('/{notification}', [NotificationController::class,'destroy'])->name('delete');
    });

    Route::get('/follow/{post}',[FollowerController::class,'follow'])->name('follow');
    Route::get('/un-follow/{post}',[FollowerController::class,'unFollow'])->name('unfollow');

});

Route::fallback(function () {
    return view('home');
});















//
Route::get('test',function () {
    $users = \App\Models\Post::find(19)->comments;
    $users = \App\Models\Comment::all();
    dd($users);

});
