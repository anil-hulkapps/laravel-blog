<?php

use App\Http\Controllers\api\UserController;
use App\Http\Controllers\api\PostController;
use App\Http\Controllers\api\CommentController;
use App\Http\Resources\CommentCollection;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login', [UserController::class,'login'])->name('login');
Route::post('register', [UserController::class,'register'])->name('register');
Route::post('refresh-token', [UserController::class,'refreshToken'])->name('refreshToken');
Route::get('/unauthorized', [UserController::class,'unauthorized'])->name('unauthorized');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('logout', [UserController::class,'logout']);
    Route::post('details', [UserController::class,'details']);


    Route::group(['prefix'=>'posts', 'as'=>'post.'], function () {
        Route::post('/', [PostController::class,'index'])->name('index');
        Route::post('/store', [PostController::class,'store'])->name('store');

        Route::group(['prefix'=>'{post}'] ,function () {
            Route::put('/', [PostController::class,'update'])->name('update');
            Route::post('/', [PostController::class,'show'])->name('show');
            Route::delete('/', [PostController::class,'destroy'])->name('destroy');
        });
    });

    Route::post('/comments',function () {
        return new CommentCollection(Comment::all());
    });
});
