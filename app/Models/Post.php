<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tag')->using(PostTag::class);
    }

    protected $casts = [
        'images'=>'array'
    ];

    public function getTitleAttribute($value)
    {
        return strtoupper($value);
    }


//    protected $attributes = [
//        'is_adult' => '0',
//    ];

}
