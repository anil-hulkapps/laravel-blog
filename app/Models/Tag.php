<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Tag extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'tag_user','tag_id','user_id');
    }

    public function setTagAttribute($value)
    {
        $this->attributes['tag'] = Str::slug($value, '-');;
    }
}
