<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Notifications\CommentNotification;
use App\Notifications\CommentReplyNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $comment->body = $request['comment_body'];
        $comment->user()->associate(Auth::user());
        $post = Post::find($request['post_id']);
        $post->comments()->save($comment);

//        $user = $post->user;
//        $baseUrl = route('post.show',$request['post_id']);
//        if ($user <> Auth::user())
//        {
//            $user->notify(new CommentNotification(Auth::user()->id, $baseUrl));
//        }

        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Comment();
        $reply->body = $request['comment_body'];
        $reply->user()->associate($request->user());
        $reply->parent_id = $request['comment_id'];
        $post = Post::find($request['post_id']);
        $post->comments()->save($reply);

        $parent_comment = Comment::find($request['comment_id']);
        $parent_comment_user = User::find($parent_comment->user_id);
        $user = $post->user;

        $baseUrl = route('post.show',$request['post_id']);
        if ($user <> Auth::user())
        {
            $user->notify(new CommentNotification(Auth::user()->id, $baseUrl));
        }

        if ($parent_comment_user <> $request->user()) {
            $parent_comment_user->notify(new CommentReplyNotification(Auth::user()->id, $baseUrl));
        }

        return back();

    }

    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);
        return view('posts.comment_edit',compact('comment'));
    }

    public function update(Request $request)
    {
        $request->all();
        $comment = Comment::find($request['comment_id']);
        $this->authorize('update', $comment);
        $comment->update([
            'body'=>$request['body'],
        ]);
        return back();
    }

    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);
        $comment->delete();
        return back();
    }

}
