<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index() {

        $unreadNotifications = Auth::user()->unreadNotifications;
        $notifications = Auth::user()->readNotifications;
        return view('notifications', compact('unreadNotifications','notifications'));

    }

    public function markAsRead($id) {

        Auth::user()->unreadNotifications->where('id', $id)->markAsRead();
        return back();
    }

    public function destroy($id) {

        Auth::user()->notifications()->where('id', $id)->delete();
        return back();
    }
}
