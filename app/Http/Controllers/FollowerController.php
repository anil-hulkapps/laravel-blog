<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowerController extends Controller
{
    public function follow(Post $post) {


        $this->authorize('follow', $post);
        $author = $post->user;
//        dd(Auth::user()->followings);
//        dd(Auth::user()->followings->contains($author));
        $author->followers()->attach(Auth::id());
        return back();
    }

    public function unFollow(Post $post) {

        $this->authorize('unfollow', $post);
        $author = $post->user;
        $author->followers()->detach(Auth::id());
        return back();
    }
}
