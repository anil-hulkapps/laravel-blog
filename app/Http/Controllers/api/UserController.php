<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Validator;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\Client as OClient;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login() {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return $this->getTokenAndRefreshToken( request('email'), request('password'));
        }
        else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'dob' => 'required',
            'languages' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $password = $request->password;
//        $input = $request->all();
        $request['password'] = bcrypt($request['password']);
        $user = User::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'dob' => $request['dob'],
            'languages' => $request['languages'],
            'email' => $request['email'],
            'password' => $request['password']
        ]);
        return $this->getTokenAndRefreshToken( $user->email, $password);
    }

    public function getTokenAndRefreshToken( $email, $password) {
        $oClient = OClient::find(2);
        $http = new Client;
        $response = $http->request('POST', url('oauth/token'), [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'username' => $email,
                'password' => $password,
                'scope' => '*',
            ],
        ]);

        $result = json_decode((string) $response->getBody(), true);
        return response()->json($result, $this->successStatus);
    }

    public function refreshToken(Request $request) {
        $refresh_token = $request->header('RefreshToken');
        $oClient = OClient::find(2);
        $http = new Client;

        try {
            $response = $http->request('POST', url('oauth/token'), [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refresh_token,
                    'client_id' => $oClient->id,
                    'client_secret' => $oClient->secret,
                    'scope' => '*',
                ],
            ]);
            return json_decode((string) $response->getBody(), true);
        } catch (Exception $e) {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function details() {
        return new UserCollection(User::all());
    }

    public function logout(Request $request) {
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out']);
    }

    public function unauthorized() {
        return response()->json(['error'=>'Unauthorised'], 401);
    }
}
