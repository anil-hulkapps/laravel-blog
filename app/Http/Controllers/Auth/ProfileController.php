<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{

    public function index() {
        $tags = Tag::all();
        return view('profile.edit',compact('tags'));
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$user->id],
            'username' => ['required', 'max:255','unique:users,username,'.$user->id],
            'mobile' => ['required','max:10'],
            'languages' => ['required'],
        ]);
        if ($avatar = $request->file('avatar')) {
            $oldAvatar = Auth::user()->avatar;
            Storage::delete('public/images/'.$oldAvatar);
            $avatarName = time().'.'.$avatar->getClientOriginalExtension();
            $request['avatar']->storeAs('public/images', $avatarName);
            $user->avatar = $avatarName;
        }

        $user->name = $request['name'];
        $user->username = $request['username'];
        $user->email = $request['email'];
        $user->mobile = $request['mobile'];
        $user->dob = $request['dob'];
        $user->languages = $request['languages'];
        $user->save();

        $interests = $request['interests'];
        $interestsIds = [];
        foreach ($interests as $tag) {
            $interest = Tag::where('tag',$tag)->first();
            if ($interest) {
                $interestsIds[] = $interest->id;
            }
        }
        $user->tags()->sync($interestsIds);

        return redirect()->route('profile.index')
            ->with('success','Your Profile changed successfully!');

    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $pass = $user->password;

        if(password_verify($request['password'] ,$pass )) {
            $request->validate([
                'new_password' => ['required', 'string', 'min:8', 'confirmed' ],
            ]);
            $newPass = Hash::make($request['new_password']);
            $user->password = $newPass;
            $user->save();

            return redirect()->route('home')
                            ->with('change','Your password changed successfully!');
        }
        return back()->with('fail','Please enter correct old password!');
    }

    public function removeAvatar()
    {
        $user = Auth::user();
        $avatar = Auth::user()->avatar;
        Storage::delete('public/images/'.$avatar);
        $user->avatar = null;
        $user->save();

        return back()->with(['Your profile avatar is removed successfully!']);

    }
}
