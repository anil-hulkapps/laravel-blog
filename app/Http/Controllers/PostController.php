<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    public function index(Request $request)
    {

        $query = Post::query();
        if (request('term')) {
            $query->where('title', 'Like', '%' . request('term') . '%')
                    ->orwhereHas('tags', function ($q) use ($request) {
                            $q->where('tags.tag','Like', request('term') );
                    });
        }

        $posts = $query->latest()->with('tags')->withCount('comments')->paginate(5);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        $tags = Tag::all();
        return view('posts.create',compact('tags'));
    }

    public function store(Request $request) {
//        dd($request['tags']);
        $request->validate([
            'title' => 'required|unique:posts|max:255',
            'description' => 'required',
            'tags'=>'required',
            'images'=>'max:2',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);
        $post =  new Post;

        if ($request->hasfile('images')) {
            $images = $request->file('images');
            $img = [];
            foreach($images as $image) {
                $name = time().'.'.$image->getClientOriginalName();
                $image->storeAs('public/images/posts', $name);
                $img[] = $name;
            }
            $post->images = $img;
        }

        $post->title = $request['title'];
        $post->user_id = Auth::user()->id;
        $post->description = $request['description'];

        $post->is_adult = $request->boolean('is_adult');
        $post->save();

        $tags = $request['tags'];
        $tagIds = [];
        foreach($tags as $tagName)
        {
            $tag = Tag::firstOrCreate(['tag'=>$tagName]);
            if($tag)
            {
                $tagIds[] = $tag->id;
            }
        }
        $post->tags()->sync($tagIds);

        return redirect('posts')->with(['success'=>'Post is created successfully!']);
    }

    public function show(Post $post)
    {

        return view('posts.show', compact('post'));
    }

    public function edit(Post $post) {

        $this->authorize('update', $post);

        $tags = Tag::all();
        return view('posts.edit', compact(['post','tags']));
    }

    public function update(Request $request, Post $post) {

        $this->authorize('update', $post);

        $request->validate([
            'title' => 'required|max:255|unique:posts,title,'.$post->id,
            'description' => 'required',
            'images'=>'max:2',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);
//        if (!$request['is_adult']) {
//            $request['is_adult'] = 0;
//        }
        if ($request->hasfile('images')) {

            if ($post->images) {
                foreach($post->images as $image){
                    Storage::delete('public/images/posts/'.$image);
                }
            }
            $images = $request->file('images');
            $img = [];
            foreach($images as $image) {
                $name = time().'.'.$image->getClientOriginalName();
                $image->storeAs('public/images/posts', $name);
                $img[] = $name;
            }
            $post->images = $img;
        }
        $post->title = $request['title'];
        $post->user_id = Auth::user()->id;
        $post->description = $request['description'];
        $post->is_adult = $request->boolean('is_adult');


        $post->save();

        $tags = $request['tags'];
        $tagIds = [];
        foreach($tags as $tagName)
        {

            $tag = Tag::firstOrCreate(['tag'=>$tagName]);
            if($tag)
            {
                $tagIds[] = $tag->id;
            }

        }
        $post->tags()->sync($tagIds);

        return redirect('posts')->with(['success'=>'Post is Updated successfully!']);
    }

    public function destroy(Post $post) {
        $this->authorize('delete', $post);

        if ($post->images) {
            foreach ($post->images as $image) {
                Storage::delete('public/images/posts/' . $image);
            }
        }
        if ($comments = $post->comments) {
            foreach($comments as $comment) {
                $comment->delete();
            }
        }
        $post->delete();
        return redirect('posts')->with(['success'=>'Post is deleted successfully!']);
    }
}
