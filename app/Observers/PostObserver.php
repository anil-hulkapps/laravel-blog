<?php

namespace App\Observers;

use App\Models\Post;
use App\Models\User;
use App\Notifications\PostNotification;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function created (Post $post)
    {
        $postUrl = route('post.show',$post->id);
        //  get post tags
        $tags = request()->tags;
//        dd($tags);

        //User is not author and has tags included in post or is follower of author

        $users = User::where('id','!=',$post->user_id)
            ->where(function ($q) use ($tags)
            {
                 $q->whereHas('tags', function (Builder $query) use ($tags)
                 {
                     $query->whereIn('tag',$tags);
                  })
                 ->orWhereHas('followings', function ($q)
                 {
                     $q->where('id',Auth::id());
                });
        })->get();
//        dd($users);

        Notification::send($users,
            new PostNotification($post->user_id, $postUrl)
        );

    }

}
