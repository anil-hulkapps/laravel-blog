<?php

namespace App\Observers;

use App\Models\Comment;
use App\Models\Post;
use App\Notifications\CommentNotification;
use Illuminate\Support\Facades\Auth;

class CommentObserver
{
    /**
     * Handle the Comment "created" event.
     *
     * @param  \App\Models\Comment  $comment
     * @return void
     */
    public function created(Comment $comment)
    {
        $post = Post::find($comment->commentable_id);
        $user = $post->user;
        $baseUrl = route('post.show',$comment->commentable_id);
        if ($user <> Auth::user())
        {
            $user->notify(new CommentNotification(Auth::user()->id, $baseUrl));
        }
    }


}
