<?php

namespace App\Mail;

use App\Models\Post;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PostCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $postUrl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $postUrl)
    {
        $this->user = $user;
        $this->postUrl = $postUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.comments_notification',['user'=>$this->user, 'post_url'=>$this->postUrl]);
    }
}
