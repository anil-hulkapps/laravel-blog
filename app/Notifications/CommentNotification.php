<?php

namespace App\Notifications;

use App\Mail\PostCreatedMail;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CommentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $baseUrl;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$baseUrl)
    {
        $this->user = $user;
        $this->baseUrl = $baseUrl;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = User::find($this->user);
//        return (new PostCreatedMail($user, $this->post))->to($notifiable->email);
        return (new MailMessage)
                    ->line('Hey '. $notifiable->name. ', '  .$user->name. ' has commented on your blog')
                    ->action('See comment', $this->baseUrl)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $user = User::find($this->user);

        return [
            'creator' => $user->name.' has commented on your post',
            'baseUrl' => $this->baseUrl,
            'user' =>$notifiable->name
        ];
    }
}
