<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LastLoginAt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $event->user->last_logged_in = $event->user->current_logged_in ? $event->user->current_logged_in : Carbon::now('Asia/Kolkata');
        $event->user->current_logged_in = Carbon::now('Asia/Kolkata');
        $event->user->save();
    }
}
