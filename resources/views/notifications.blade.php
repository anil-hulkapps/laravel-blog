@extends('layouts.app')

@section('content')
<div style="text-align: center">
{{--<h3>These are notifications</h3>--}}
<br>

<ul>
@forelse ($unreadNotifications as $notification)
     <li class="alert alert-success" role="alert">{{$notification->data['creator']}}({{$notification->created_at->diffForHumans()}}) ->
         <a href="{{$notification->data['baseUrl']}}" style="color: #e74c3c">go to post</a>
         <a href="{{route('notifications.read.mark',$notification->id)}}" style="color: #2980b9; margin-left: 50px">Mark as read</a>
         <form action="{{route('notifications.delete',$notification->id)}}" method="post" class="d-inline ml-5">
             @csrf
             @method('delete')
             <button type="submit" class="btn btn-outline-danger">Delete</button>
         </form>
     </li>
@empty
    <h3 style="color: #218838">No unread notifications for you!</h3>
@endforelse
</ul>
<hr style="background: #7f8c8d; height: 10px; margin: 50px"/>
{{--<h3>Already Read notification</h3>--}}
<ul>
@forelse ($notifications as $notification)
     <li class="alert alert-danger" role="alert">{{$notification->data['creator']}} ({{$notification->created_at->diffForHumans()}}) ->
         <a href="{{$notification->data['baseUrl']}}" style="color: red">go to post</a>
         <form action="{{route('notifications.delete',$notification->id)}}" method="post" class="d-inline ml-5">
             @csrf
             @method('delete')
             <button type="submit" class="btn btn-outline-danger">Delete</button>
         </form>
     </li>
@empty
    <h5 style="color: #c0392b">No already read notifications for you!</h5>
@endforelse
</ul>
</div>
@endsection

