@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Change Profile Details') }}</div>
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="card-body">
                        <form method="POST" action="{{ route('profile.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6 input-group">
                                    <span class="input-group-text"><i class="fa fa-user"></i></span>
                                    <input id="name" type="text" class="form-control
                                           @error('name') is-invalid @enderror" name="name"
                                           value="{{ Auth::user()->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                <div class="col-md-6 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">@</span>
                                    </div>
                                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ Auth::user()->username }}" required autocomplete="username" >

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6 input-group">
                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row" id="datepicker">
                                <label for="dob" class="col-md-4 col-form-label text-md-right">{{ __('Date of Birth') }}</label>

                                <div class="col-md-6 input-group date">
                                    <input id="dob" type="text" class="form-control @error('dob') is-invalid @enderror"
                                           name="dob" value="{{Auth::user()->dob}}" required autocomplete="dob" >
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Phone No.') }}</label>

                                <div class="col-md-6 input-group">
                                    <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                    <input id="mobile" type="number" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ Auth::user()->mobile }}" required autocomplete="mobile" >

                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            @if (Auth::user()->avatar)
                                <div class="form-group row">
                                    <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Your Profile Avatar') }}</label>
                                    <div class="col-md-6 input-group">
                                          <img src="{{asset('storage/images/'.Auth::user()->avatar)}}"
                                                height="70px" width="70px" style="border-radius: 50%" id="avatar">
                                            <a href="{{route('profile.avatar.remove')}}" style="font-size: 20px; color: red">x</a>
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">{{ __('Set new avatar') }}</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control-file @error('avatar') is-invalid @enderror" name="avatar">

                                    @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="languages" class="col-md-4 col-form-label text-md-right">{{ __('Known Languages') }}</label>

                                <div class="col-md-6">
                                    <select name="languages[]" id="languages" class="form-select  languages @error('languages') is-invalid @enderror" multiple>
                                        <option {{ in_array('english',Auth::user()->languages )? 'selected':'' }} value="english" >English</option>
                                        <option {{ in_array('hindi',Auth::user()->languages )? 'selected':'' }} value="hindi">Hindi</option>
                                        <option {{ in_array('gujarati',Auth::user()->languages )? 'selected':'' }} value="gujarati">Gujarati</option>
                                    </select>

                                    @error('languages')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="interests" class="col-md-4 col-form-label text-md-right">{{ __('Pick Your Interests') }}</label>

                                <div class="col-md-6">
                                    <select name="interests[]" id="interests" class="form-select interests" multiple>
                                        @foreach ($tags as $tag)
                                            <option value="{{$tag->tag}}"
                                                {{ Auth::user()->tags && in_array($tag->tag,Auth::user()->tags->pluck('tag')->toArray() )
                                                ? 'selected':'' }}>
                                                {{$tag->tag}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update Profile') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $.noConflict();
            $( "#dob" ).datepicker();
        } );
    </script>

@endsection
