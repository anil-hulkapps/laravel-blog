@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-6">
        <!-- Profile Image -->
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
                @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle-sm"
                         src="storage/images/{{Auth::user()->avatar ?? 'user.png'}}"
                         alt="User profile picture" height="40px" width="40px" style="border-radius : 50% ">
                    <h3 class="profile-username font-weight-bold">{{Auth::user()->name}}</h3>
                    <h5>({{Auth::user()->is_admin? 'Admin' : 'User'}})</h5>
                </div>

            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->
        <div class="card card-primary">
            <div class="card-header row justify-content-center">
                <h3 class="card-title">About Me</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <strong>Username</strong>
                <p class="text-muted">{{Auth::user()->username}}</p>
                <hr>

                <strong>Email</strong>
                <p class="text-muted">{{Auth::user()->email}}</p>
                <hr>
                <strong>Mobile no.</strong>
                <p class="text-muted">{{Auth::user()->mobile ?? 'Please Add Mobile No. in change Profile'}}</p>
                <hr>
                <strong>Age</strong>
                @php
                    use Carbon\Carbon;
                    $date = Auth::user()->dob ;
                    $dt = Carbon::parse($date);
                    $year = Carbon::createFromDate($dt->year, $dt->month, $dt->day)->diff(Carbon::now())->format('%y years,%m months,%d days');
                @endphp
                <p class="text-muted">{{$year??''}}</p>
                <hr>

                <strong>known languages</strong>
                <p class="text-muted">
                    @foreach(auth()->user()->languages ?? ['please choose languages'] as $language)
                    <span>{{$language}},</span>
                    @endforeach
                </p>
                <hr>

                <strong>Interested In</strong>
                <p class="text-muted">
                    @if (auth()->user()->tags)
                        @foreach(auth()->user()->tags as $interest)
                            <span>{{$interest->tag}},</span>
                        @endforeach
                    @endif
                </p>
                <hr>

            </div>
            <a href="{{route('profile.edit')}}" class="btn btn-primary btn-block"><b>Change Profile</b></a>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    </div>
@endsection

