@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if($msg = Session::get('success'))
                    <div class="alert alert-success">
                        {{$msg}}
                    </div>
                @endif
                    <div class="mx-auto pull-left">
                            <form action="{{ route('post.index') }}" method="GET" role="search">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a href="{{ route('post.index') }}">
                                         <span class="input-group-btn">
                                             <button class="btn btn-outline-danger" type="button" title="Refresh page">
                                                 <span class="fa fa-refresh"></span>
                                             </button>
                                        </span>
                                    </a>
                                     </span>
                                    <input type="text" class="form-control" name="term" placeholder="Search posts" id="term">
                                    <button class="btn btn-outline-info" type="submit" title="Search posts">
                                        <span class="fa fa-search"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                <div class="float-right mb-3">
                    <a href="{{route('post.create')}}" class="btn btn-success"><i class="fa fa-plus-circle" ></i></a>
                </div>
                <table class="table table-striped mt-3">
                    <thead>
                    <th>Title</th>
                    <th class="actions"> Action</th>
                    </thead>
                    <tbody>
                    @forelse($posts as $post)
                        <tr>
                            <td class="description"><h3><strong  @if ($post->is_adult == 1)
                                             class="badge badge-danger"
                                             @endif class="badge badge-secondary">{{ $post->title }}</strong>
                                </h3>
                                <p>Author : {{$post->user->name}}</p>
                                <p>Created_at : {{$post->created_at->diffForHumans()}}</p>
                                <p>Comments : {{$post->comments_count}}</p>
                                @foreach($post->tags as $tag)
                                    <h5 class="d-inline"><span class="badge badge-success">{{ $tag->tag }}</span></h5>
                                @endforeach
                            </td>
                            <td class="actions">

                                <form action="{{route('post.destroy', $post->id)}}"  method="post">
                                    @csrf
                                    <a href="{{route('post.show',  $post->id)}}" class="btn btn-outline-success"><i class="fa fa-eye"></i></a>
                                    @can('update', $post)
                                         <a href="{{route('post.edit',  $post->id)}}" class="btn btn-outline-primary"><i class="fa fa-edit"></i></a>
                                    @endcan
                                    @method('delete')
                                    @can('update', $post)
                                        <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                                    @endcan
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td><h3>No post found!</h3></td>
                        </tr>
                    @endforelse
                    </tbody>

                </table>
                    {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection

@section('css')
p{
line-height: 10px;
}
table {
width: 100%;
table-layout: fixed;
}

.actions {
max-width:10%;
text-align: right;
padding-right:4px;
}

.description {
text-overflow: ellipsis;
white-space: nowrap;
overflow: hidden;
width:80%;
max-width:80%;
}
@endsection
