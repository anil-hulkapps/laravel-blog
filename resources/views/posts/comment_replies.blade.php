
@foreach($comments as $comment)
    <div class="display-comment" class="d-inline-flex">
        @if (Auth::user()->avatar)
            <img src="{{asset('storage/images/'.Auth::user()->avatar)}}"
                 height="30px" width="30px" style="border-radius: 50%">
        @else
            <img src="{{asset('storage/images/user.png')}}"
                 height="30px" width="30px" style="border-radius: 50%">
        @endif
        <strong>{{ $comment->user->name }}</strong>
        <p>{{ $comment->body }} <span style="float: right"><i class="fa fa-calendar"></i>{{$comment->created_at->diffForHumans()}}</span></p>


        <div style="display: flex">
            <form method="post" action="{{ route('comment.reply.add') }}" >
                @csrf
                <div class="form-group">
                    <input type="text" name="comment_body" class="form-control" />
                    <input type="hidden" name="post_id" value="{{ $post_id }}" />
                    <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-outline-info"><i class="fa fa-reply"></i></button>
                </div>
            </form>
            @can('update', $comment)
                    <a>
                        <button class="btn btn-outline-success editBtn" data-toggle="modal" data-target="#exampleModal"
                            data-body="{{$comment->body}}" data-id="{{$comment->id}}" >
                        <i class="fa fa-edit"></i>
                        </button>
                    </a>
            @endcan
            @can('delete', $comment)
                    <form action="{{route('comment.delete',$comment->id)}}" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button>
                    </form>
            @endcan
        </div>
        @include('posts.comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modify Your Comment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" action="{{ route('comment.update','comment') }}">
{{--                    @csrf--}}
                    {!! csrf_field() !!}
                    @method('put')
                    <div class="form-group row">
                        <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Change Comment') }}</label>

                        <div class="col-md-6">
                            <input id="comment" type="text" class="form-control" name="body" >
                            <input id="comment_id" type="hidden" class="form-control" name="comment_id" >
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Change comment') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
