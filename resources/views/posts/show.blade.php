@extends('layouts.app')

@section('content')
    <div class="container">
        <a href="{{route('post.index')}}" class="btn btn-outline-primary float-right"><i class="fa fa-chevron-left" ></i></a>
        <div class="row justify-content-center">
            <div class="col-md-8">

                <div class="card">
                    <div class="card-body">
                            <h3><b>{{ $post->title }}</b></h3>
                            <p class="d-inline">Author : <strong>{{$post->user->name}}</strong></p>
                        @can('follow', $post)
                            <h3 class="float-right"><a href="{{route('follow',$post->id)}}"><span class="badge badge-success">Follow</span></a></h3>
                        @endcan
                        @can('unfollow', $post)
                              <h3 class="float-right"><a href="{{route('unfollow',$post->id)}}" ><span class="badge badge-danger">UnFollow</span></a></h3>
                        @endcan
                            <br>
                        <hr>
                        <hr>
                        @if ($post->images)
                            @foreach($post->images as $image)
                                <img src="{{asset('storage/images/posts/'.$image)}}" alt="featured" width="300px" >
                            @endforeach
                        @endif
                        <p>{{ $post->description }}</p>
                        <hr style="background: #1c7430; height: 5px"/>
                        <h4>Comments</h4>
                        <hr style="background: #1c7430; height: 5px"/>

                        @include('posts.comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])

                        <h4>Add comment</h4>
                        <form method="post" action="{{ route('comment.store') }}">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="comment_body" class="form-control" />
                                <input type="hidden" name="post_id" value="{{ $post->id }}" />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-warning" value="Add Comment" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





@section('scripts')

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" ></script>
    <script>
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var body = button.data('body');
            var id = button.data('id');
            var modal = $(this)

            modal.find('.modal-body #comment').val(body);
            modal.find('.modal-body #comment_id').val(id);
        })

    </script>
@endsection
