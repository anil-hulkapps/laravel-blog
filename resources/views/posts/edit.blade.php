@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h2 class="d-inline">Edit Post</h2>
                        <a href="{{route('post.index')}}" class="btn btn-outline-primary float-right"><i class="fa fa-chevron-left" ></i></a></div>
                    <div class="card-body">
                        <form method="post" action="{{ route('post.update',$post->id) }}" enctype="multipart/form-data">
                            <div class="form-group">
                                @csrf
                                @method('put')
                                <label class="label" for="title">Post Title: </label>
                                <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}"/>
                            </div>
                            <div class="form-group">
                                <label class="label" for="body">Post Description: </label>
                                <textarea name="description" id="body" rows="10" cols="30" class="form-control" required>{{$post->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label class="label" for="tags">Tags </label>
                                <select name="tags[]" id="tags" class="form-select form-control tags @error('tags') is-invalid @enderror" multiple>
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->tag}}" @if (in_array($tag->tag,$post->tags->pluck('tag')->all()))
                                                selected
                                                @endif >
                                            {{$tag->tag}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                @if ($post->images)
                                    @foreach($post->images as $image)
                                        <img src="{{asset('storage/images/posts/'.$image)}}" alt="featured" width="100px">
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label class="label" for="images">Change Images: </label>
                                <input type="file" name="images[]" id="images" class="form-control-file" multiple/>
                            </div>
                            <div class="form-check form-group">
                                <input class="form-check-input" type="checkbox" name="is_adult" value="1" id="adult"
                                        @if ($post->is_adult == 1)
                                            checked
                                        @endif>
                                <label class="form-check-label" for="adult">
                                    18+ Content
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Update" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('scripts')--}}
{{--    <script>--}}
{{--        $(document).ready(function() {--}}
{{--            $(function () {--}}
{{--                $("input[type = 'submit']").on('click',function (e) {--}}
{{--                    let $fileUpload = $("input[type='file']");--}}
{{--                    // console.log(parseInt($fileUpload.get(0).files.length));--}}
{{--                    if (parseInt($fileUpload.get(0).files.length) > 2) {--}}
{{--                        alert("You are only allowed to upload a maximum of 3 files");--}}
{{--                        e.preventDefault();--}}
{{--                    }--}}
{{--                });--}}
{{--            });--}}
{{--        });--}}
{{--    </script>--}}
{{--@endsection--}}
