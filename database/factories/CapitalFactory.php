<?php

namespace Database\Factories;

use App\Models\Capital;
use Illuminate\Database\Eloquent\Factories\Factory;

class CapitalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Capital::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
